// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'marketplace_record.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<MarketplaceRecord> _$marketplaceRecordSerializer =
    new _$MarketplaceRecordSerializer();

class _$MarketplaceRecordSerializer
    implements StructuredSerializer<MarketplaceRecord> {
  @override
  final Iterable<Type> types = const [MarketplaceRecord, _$MarketplaceRecord];
  @override
  final String wireName = 'MarketplaceRecord';

  @override
  Iterable<Object?> serialize(Serializers serializers, MarketplaceRecord object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.name;
    if (value != null) {
      result
        ..add('name')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.description;
    if (value != null) {
      result
        ..add('description')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.email;
    if (value != null) {
      result
        ..add('email')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.busCellPhone;
    if (value != null) {
      result
        ..add('bus_cell_phone')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.closeTime;
    if (value != null) {
      result
        ..add('close_time')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.openTime;
    if (value != null) {
      result
        ..add('open_time')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.ffRef;
    if (value != null) {
      result
        ..add('Document__Reference__Field')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(
                DocumentReference, const [const FullType.nullable(Object)])));
    }
    return result;
  }

  @override
  MarketplaceRecord deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new MarketplaceRecordBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'description':
          result.description = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'email':
          result.email = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'bus_cell_phone':
          result.busCellPhone = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'close_time':
          result.closeTime = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'open_time':
          result.openTime = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'Document__Reference__Field':
          result.ffRef = serializers.deserialize(value,
              specifiedType: const FullType(DocumentReference, const [
                const FullType.nullable(Object)
              ])) as DocumentReference<Object?>?;
          break;
      }
    }

    return result.build();
  }
}

class _$MarketplaceRecord extends MarketplaceRecord {
  @override
  final String? name;
  @override
  final String? description;
  @override
  final String? email;
  @override
  final String? busCellPhone;
  @override
  final String? closeTime;
  @override
  final String? openTime;
  @override
  final DocumentReference<Object?>? ffRef;

  factory _$MarketplaceRecord(
          [void Function(MarketplaceRecordBuilder)? updates]) =>
      (new MarketplaceRecordBuilder()..update(updates))._build();

  _$MarketplaceRecord._(
      {this.name,
      this.description,
      this.email,
      this.busCellPhone,
      this.closeTime,
      this.openTime,
      this.ffRef})
      : super._();

  @override
  MarketplaceRecord rebuild(void Function(MarketplaceRecordBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MarketplaceRecordBuilder toBuilder() =>
      new MarketplaceRecordBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is MarketplaceRecord &&
        name == other.name &&
        description == other.description &&
        email == other.email &&
        busCellPhone == other.busCellPhone &&
        closeTime == other.closeTime &&
        openTime == other.openTime &&
        ffRef == other.ffRef;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc($jc($jc(0, name.hashCode), description.hashCode),
                        email.hashCode),
                    busCellPhone.hashCode),
                closeTime.hashCode),
            openTime.hashCode),
        ffRef.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'MarketplaceRecord')
          ..add('name', name)
          ..add('description', description)
          ..add('email', email)
          ..add('busCellPhone', busCellPhone)
          ..add('closeTime', closeTime)
          ..add('openTime', openTime)
          ..add('ffRef', ffRef))
        .toString();
  }
}

class MarketplaceRecordBuilder
    implements Builder<MarketplaceRecord, MarketplaceRecordBuilder> {
  _$MarketplaceRecord? _$v;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  String? _description;
  String? get description => _$this._description;
  set description(String? description) => _$this._description = description;

  String? _email;
  String? get email => _$this._email;
  set email(String? email) => _$this._email = email;

  String? _busCellPhone;
  String? get busCellPhone => _$this._busCellPhone;
  set busCellPhone(String? busCellPhone) => _$this._busCellPhone = busCellPhone;

  String? _closeTime;
  String? get closeTime => _$this._closeTime;
  set closeTime(String? closeTime) => _$this._closeTime = closeTime;

  String? _openTime;
  String? get openTime => _$this._openTime;
  set openTime(String? openTime) => _$this._openTime = openTime;

  DocumentReference<Object?>? _ffRef;
  DocumentReference<Object?>? get ffRef => _$this._ffRef;
  set ffRef(DocumentReference<Object?>? ffRef) => _$this._ffRef = ffRef;

  MarketplaceRecordBuilder() {
    MarketplaceRecord._initializeBuilder(this);
  }

  MarketplaceRecordBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _name = $v.name;
      _description = $v.description;
      _email = $v.email;
      _busCellPhone = $v.busCellPhone;
      _closeTime = $v.closeTime;
      _openTime = $v.openTime;
      _ffRef = $v.ffRef;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(MarketplaceRecord other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$MarketplaceRecord;
  }

  @override
  void update(void Function(MarketplaceRecordBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  MarketplaceRecord build() => _build();

  _$MarketplaceRecord _build() {
    final _$result = _$v ??
        new _$MarketplaceRecord._(
            name: name,
            description: description,
            email: email,
            busCellPhone: busCellPhone,
            closeTime: closeTime,
            openTime: openTime,
            ffRef: ffRef);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,no_leading_underscores_for_local_identifiers,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new,unnecessary_lambdas
