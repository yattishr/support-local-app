import 'dart:async';

import 'index.dart';
import 'serializers.dart';
import 'package:built_value/built_value.dart';

part 'marketplace_record.g.dart';

abstract class MarketplaceRecord
    implements Built<MarketplaceRecord, MarketplaceRecordBuilder> {
  static Serializer<MarketplaceRecord> get serializer =>
      _$marketplaceRecordSerializer;

  String? get name;

  String? get description;

  String? get email;

  @BuiltValueField(wireName: 'bus_cell_phone')
  String? get busCellPhone;

  @BuiltValueField(wireName: 'close_time')
  String? get closeTime;

  @BuiltValueField(wireName: 'open_time')
  String? get openTime;

  @BuiltValueField(wireName: kDocumentReferenceField)
  DocumentReference? get ffRef;
  DocumentReference get reference => ffRef!;

  static void _initializeBuilder(MarketplaceRecordBuilder builder) => builder
    ..name = ''
    ..description = ''
    ..email = ''
    ..busCellPhone = ''
    ..closeTime = ''
    ..openTime = '';

  static CollectionReference get collection =>
      FirebaseFirestore.instance.collection('marketplace');

  static Stream<MarketplaceRecord> getDocument(DocumentReference ref) => ref
      .snapshots()
      .map((s) => serializers.deserializeWith(serializer, serializedData(s))!);

  static Future<MarketplaceRecord> getDocumentOnce(DocumentReference ref) => ref
      .get()
      .then((s) => serializers.deserializeWith(serializer, serializedData(s))!);

  MarketplaceRecord._();
  factory MarketplaceRecord([void Function(MarketplaceRecordBuilder) updates]) =
      _$MarketplaceRecord;

  static MarketplaceRecord getDocumentFromData(
          Map<String, dynamic> data, DocumentReference reference) =>
      serializers.deserializeWith(serializer,
          {...mapFromFirestore(data), kDocumentReferenceField: reference})!;
}

Map<String, dynamic> createMarketplaceRecordData({
  String? name,
  String? description,
  String? email,
  String? busCellPhone,
  String? closeTime,
  String? openTime,
}) {
  final firestoreData = serializers.toFirestore(
    MarketplaceRecord.serializer,
    MarketplaceRecord(
      (m) => m
        ..name = name
        ..description = description
        ..email = email
        ..busCellPhone = busCellPhone
        ..closeTime = closeTime
        ..openTime = openTime,
    ),
  );

  return firestoreData;
}
