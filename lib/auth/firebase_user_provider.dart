import 'package:firebase_auth/firebase_auth.dart';
import 'package:rxdart/rxdart.dart';

class SupportLocalMtn2FirebaseUser {
  SupportLocalMtn2FirebaseUser(this.user);
  User? user;
  bool get loggedIn => user != null;
}

SupportLocalMtn2FirebaseUser? currentUser;
bool get loggedIn => currentUser?.loggedIn ?? false;
Stream<SupportLocalMtn2FirebaseUser> supportLocalMtn2FirebaseUserStream() =>
    FirebaseAuth.instance
        .authStateChanges()
        .debounce((user) => user == null && !loggedIn
            ? TimerStream(true, const Duration(seconds: 1))
            : Stream.value(user))
        .map<SupportLocalMtn2FirebaseUser>(
            (user) => currentUser = SupportLocalMtn2FirebaseUser(user));
