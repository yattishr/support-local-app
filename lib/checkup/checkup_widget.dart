import '../auth/auth_util.dart';
import '../auth/firebase_user_provider.dart';
import '../createmarketplace/createmarketplace_widget.dart';
import '../flutter_flow/flutter_flow_theme.dart';
import '../flutter_flow/flutter_flow_util.dart';
import '../main.dart';
import '../marketplace_dashboard2/marketplace_dashboard2_widget.dart';
import '../onboarding/onboarding_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:google_fonts/google_fonts.dart';

class CheckupWidget extends StatefulWidget {
  const CheckupWidget({Key? key}) : super(key: key);

  @override
  _CheckupWidgetState createState() => _CheckupWidgetState();
}

class _CheckupWidgetState extends State<CheckupWidget> {
  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    // On page load action.
    SchedulerBinding.instance.addPostFrameCallback((_) async {
      await Future.delayed(const Duration(milliseconds: 1000));
      if (loggedIn) {
        if (FFAppState().firstTime) {
          await Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => OnboardingWidget(),
            ),
          );
        } else {
          if (valueOrDefault<bool>(currentUserDocument?.isAdmin, false)) {
            await Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => CreatemarketplaceWidget(),
              ),
            );
          } else {
            await Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => NavBarPage(initialPage: 'HomePage'),
              ),
            );
          }
        }
      } else {
        if (valueOrDefault<bool>(currentUserDocument?.isAdmin, false)) {
          await Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => MarketplaceDashboard2Widget(),
            ),
          );
        } else {
          await Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => NavBarPage(initialPage: 'HomePage'),
            ),
          );
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: FlutterFlowTheme.of(context).primaryBackground,
      body: SafeArea(
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Container(
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
              color: FlutterFlowTheme.of(context).primaryBackground,
            ),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  'assets/images/albfo_3.png',
                  width: 250,
                  height: 250,
                  fit: BoxFit.cover,
                ),
                Text(
                  'Checkup...',
                  style: FlutterFlowTheme.of(context).title1,
                ),
                Text(
                  'Wait...',
                  style: FlutterFlowTheme.of(context).title3,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
